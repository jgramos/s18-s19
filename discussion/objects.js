//Javascript Object

let cellphone = {
	name: 'Nokia 3210',
	manufactureDate: '1999',
	price: 1000
}; //'Key' -> is also mostly referred to as 'property' of an object.

console.log(cellphone);
console.log(typeof cellphone);
// How to store multiple array
// you can use an array structure to store them.
let users = [
	{ 
		name: 'Anna', 
		age: '23'
	},
	{ 
		name: 'Nicole', 
		age: '18'
	},
	{ 
		name: 'Smith', 
		age: '42'
	},
	{ 
		name: 'Pam', 
		age: '13'
	},
	{ 
		name: 'Anderson', 
		age: '26'
	}
];

console.log(users);
console.table(users);

 //Complex examples of JS objects

//NOTE: Different data types may be stored in an object's property creating more complex data structures

//When using JS Objects, you can also insert 'Methods'.

//Methods -> are useful for creating reusable functions that can perform tasks related to an object. 
let friend = {
	 //properties
	firstName: 'Joe', //String or numbers
	lastName: 'Smith', 
	isSingle: true, //boolean
 	emails: ['joesmith@gmail.com', 'jsmith@company.com'], //array
 	address: { //Objects
 	city: 'Austin',
 	state: 'Texas',
 	country: 'USA'
 },
 //methods 
 introduce: function() {
 	console.log('Hello Meet my new Friend'); 
 },
 advice: function() {
 	console.log('Give friend an advice to move on');
 }, 
 wingman: function() {
 	console.log('Hype friend when meeting new people');
 }
}
console.log(friend);

//[SECTION] ALTERNATE APPROACHES on HOW TO DECLARE AN OBJECT IN JAVASCRIPT.
	//1. How to create an Object using a Constructor function?
	// Constructor function -> we will need t create a reusable function in which we will declare a blueprint to describe the anatomy of an object that we wish to create.

	// To declare properties within a constructor function, you have to be familiar with the 'this' keyword
	function Laptop(name, year, model) {
		this.name = name;
		this.manufacturedOn = year;
		this.model = model;
	};

	//to create a new object that has the properties stated above, we will use the 'new' keyword for u to be able to create a new instance of an object.

	let laptop1 = new Laptop('Vivo', 2018, 'MMR');
	let laptop2 = new Laptop('Oppo', 2020,'9165');
	let laptop3 = new Laptop('Xiami', 2021, '1602');

	let gadgets = [laptop1, laptop2, laptop3];
	console.table(gadgets);

	//example2:

	//lets create an instance of multiple pokemon characters

	function Pokemon(name, type , level, trainer) {
		//properties
		this.pokemonName = name; 
		this.pokenmonType = type;
		this.pokemonHealth = 2 * level;
		this.pokemonLevel = level;
		this.owner = trainer;
		//methods
		this.tacke = function(target) {
			console.log(this.pokemonName + ' tackled ' + target.pokemonName);
		}
		this.greetings = function() {
			console.log(this.pokemonName + ' says Hello!');
		}
	}

	//create instance of a pokemon using a constructor function.
	let pikachu = new Pokemon('Pikachu', 'electric', 3, 'Ash ketchup');
	let ratata = new Pokemon('Ratata', 'normal', 4, 'Misty');
	let snorlx = new Pokemon('Snorlax', 'normal', 5, 'Gary Oak');
	//display all pokemons in the console in a table format
	let pokemons = [pikachu, ratata, snorlx];
	console.table(pokemons);

	//[SECTION] How to access Object Properties?
		//using '.' dot notation you can access properties of an object
		//SYNTAX: objectName.propertyName.
	console.log(snorlx.owner);
	pikachu.tacke(ratata);
	snorlx.greetings();

	console.log(ratata['owner']);
	console.log(ratata['pokemonLevel'])

	//[ADDITIONAL KNOWLEDGE]:
		//KEEP this in mind, you can write and declare objects this way:

		//to create an objects, you will use object literals '{}'
		let trainer = {};//empty object
		console.log(trainer);

		//will I be able to add properties from outside an ojbect? YES
		trainer.name = 'Ash Ketchum';
		console.log(trainer);
		trainer.friends = ['Misty','Brock','Tracey'];
		console.log(trainer);
		//method
		trainer.speak = function() {
			console.log('Pikachu, I choose you!');
		}
		trainer.speak();
